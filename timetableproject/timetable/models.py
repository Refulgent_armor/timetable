from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import MinLengthValidator, MaxValueValidator, MinValueValidator
from django.conf import settings


MAX_NUMBER_OF_LESSON_IN_DAY = 15
MAX_NUMBER_WEEK_IN_HALF_YEAR = 28


class Customer(models.Model):
    first_name = models.CharField('First name', max_length=255)
    last_name = models.CharField('Last name', max_length=255)
    email = models.EmailField()
    phone = models.CharField(max_length=20)
    address = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    createdAt = models.DateTimeField('Created At', auto_now_add=True)

    def __str__(self):
        return self.first_name


# Расширяем модель пользователя на будущее (CUser = CustomUser)
class CUser(AbstractUser):
    pass

    # add additional fields in here

    def __str__(self):
        return self.username


# Преподаватель
class Teacher(models.Model):
    first_name = models.CharField(max_length=40, validators=[MinLengthValidator(2)])
    last_name = models.CharField(max_length=40, validators=[MinLengthValidator(2)])
    patronymic = models.CharField(max_length=40, validators=[MinLengthValidator(2)])
    photo = models.ImageField(upload_to='Teacher', null=True, default=None)
    bio = models.TextField(default='')
    user_account = models.OneToOneField(settings.AUTH_USER_MODEL, null=True, default=None, on_delete=models.SET_NULL,
                                        related_name='teacher')
    rank = models.ForeignKey('Rank', null=True, default=None, on_delete=models.SET_NULL, related_name='teachers')
    academic_degree = models.ForeignKey('AcademicDegree', null=True, default=None, on_delete=models.SET_NULL,
                                        related_name='teachers')
    job_position = models.ManyToManyField('JobPosition', null=True, default=None, related_name='teachers')


# Учебная кафедра
class EducationDepartment(models.Model):
    # Код кафедры (сокращение)
    code = models.CharField(max_length=40)
    # Полное наименование кафедры
    name = models.CharField(max_length=150)
    description = models.TextField(default='')

    def __str__(self):
        return 'Education department {}'.format(self.code)


# Должность
class JobPosition(models.Model):
    short_name = models.CharField(max_length=40)
    name = models.CharField(max_length=150)
    education_department = models.ForeignKey('EducationDepartment', on_delete=models.PROTECT,
                                             related_name='job_positions')


# Учёная степень
class AcademicDegree(models.Model):
    short_name = models.CharField(max_length=40)
    name = models.CharField(max_length=150)


# Звание
class Rank(models.Model):
    short_name = models.CharField(max_length=40)
    name = models.CharField(max_length=150)


# Учебный предмет (подразумевается курс)
class Subject(models.Model):
    name = models.CharField(max_length=40)
    full_name = models.CharField(max_length=40)
    code = models.CharField(max_length=40)
    description = models.TextField(default='')
    full_description = models.TextField(default='')


# Тип занятия
class LessonType(models.Model):
    name = models.CharField(max_length=40)
    description = models.TextField(default='')


# Занятие
class Lesson(models.Model):
    subject = models.ForeignKey('Subject', on_delete=models.CASCADE, related_name='lessons')
    lesson_type = models.ForeignKey('LessonType', on_delete=models.PROTECT, related_name='lessons')


# Занятие преподавателя
class TeacherLesson(models.Model):
    teacher = models.ForeignKey('Teacher', on_delete=models.PROTECT, related_name='lessons')
    lesson = models.ForeignKey('Lesson', on_delete=models.PROTECT, related_name='lessons')


# Учебная пара
class GroupClass(models.Model):
    # нулевое значение подразумевает отсутствие подгруппы
    subgroup = models.PositiveSmallIntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(20)])
    teacher_lesson = models.ForeignKey('TeacherLesson', on_delete=models.CASCADE, related_name='classes')
    class_room = models.ForeignKey('ClassRoom', null=True, on_delete=models.SET_NULL, related_name='classes')
    education_group = models.ManyToManyField('EducationGroup', null=True, related_name='classes')


# Форма обучения
class EducationForm(models.Model):
    name = models.CharField(max_length=40)

    def __str__(self):
        return 'Education form {}'.format(self.name)


# Специальность
class Specialty(models.Model):
    # код специальности
    code = models.CharField(max_length=40)
    # текстовое наименование
    name = models.CharField(max_length=150)
    description = models.TextField(default='')
    education_department = models.ForeignKey('EducationDepartment', on_delete=models.PROTECT,
                                             related_name='specialties')

    def __str__(self):
        return 'Specialty {}'.format(self.code)


# Здание
class Building(models.Model):
    name = models.CharField(max_length=40)
    address = models.CharField(max_length=150)
    description = models.TextField(default='')
    photo = models.ImageField(upload_to='buildings', null=True, default=None)


# Кабинет
class ClassRoom(models.Model):
    name = models.CharField(max_length=40)
    description = models.TextField(default='')
    photo = models.ImageField(upload_to='classrooms', null=True, default=None)
    building = models.ForeignKey('Building', on_delete=models.PROTECT, related_name='class_rooms')


# Группа
class EducationGroup(models.Model):
    name = models.CharField(max_length=40)
    education_form = models.ForeignKey('EducationForm', on_delete=models.PROTECT, related_name='education_groups')
    specialty = models.ForeignKey('Specialty', on_delete=models.PROTECT, related_name='education_groups')


# Расписание
class Timetable(models.Model):
    group_class = models.ForeignKey('GroupClass', on_delete=models.CASCADE, related_name='classes_in_timetable')
    date = models.DateField()
    week = models.PositiveSmallIntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(MAX_NUMBER_WEEK_IN_HALF_YEAR)])
    day_of_week = models.PositiveSmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(7)])
    number_of_lesson_in_day = models.PositiveSmallIntegerField(validators=[MinValueValidator(1),
                                                                           MaxValueValidator(MAX_NUMBER_OF_LESSON_IN_DAY)])


# Время Пар
class ClassTime(models.Model):
    # 1, 2, 3 пара
    number_in_order = models.PositiveSmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(20)])
    # 9h,30m = 540+30 = 570 minutes (begin of ClassTime)
    minutes_time_begin = models.PositiveSmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(1440)])
    # 10h,20m = 600+20 = 620 minutes (end of ClassTime)
    minutes_time_end = models.PositiveSmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(1440)])


# Дата начала семестра
class DateOfStartEducation(models.Model):
    FIRST_HALF = False
    SECOND_HALF = True
    HALF_YEAR_CHOICES = [
        (FIRST_HALF, 'First half year (autumn - winter)'),
        (SECOND_HALF, 'Second half year (winter - spring)')
    ]
    # дата начала семестра (1.09.2020)
    date = models.DateField()
    half_year = models.BooleanField(choices=HALF_YEAR_CHOICES)


# Перенос
class TimetableTransfer(models.Model):
    education_hours = models.ForeignKey('Timetable', on_delete=models.CASCADE)
    date = models.DateField()
    week = models.PositiveSmallIntegerField(default=0, validators=[MaxValueValidator(MAX_NUMBER_WEEK_IN_HALF_YEAR)])
    day_of_week = models.PositiveSmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(7)])
    number_of_lesson_in_day = models.PositiveSmallIntegerField(validators=[MinValueValidator(1),
                                                                           MaxValueValidator(MAX_NUMBER_OF_LESSON_IN_DAY)])
    creator = models.ForeignKey('Teacher', null=True, on_delete=models.SET_NULL)


# Заявка
class TimetableChangeRequest(models.Model):
    PENDING = 1
    SATISFIED = 2
    REJECTED = 3
    TIMETABLE_CHANGE_REQUEST_STATUS = [
        (PENDING, 'В ожидании ответа'),
        (SATISFIED, 'Удовлетоврено'),
        (REJECTED, 'Отклонено')
    ]
    text = models.TextField(default='')
    answer = models.TextField(default='')
    status = models.PositiveSmallIntegerField(choices=TIMETABLE_CHANGE_REQUEST_STATUS)


# Комментарий
class TimetableComment(models.Model):
    education_hours = models.ForeignKey('Timetable', on_delete=models.CASCADE)
    text = models.TextField(default='')
