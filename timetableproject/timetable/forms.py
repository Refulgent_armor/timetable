from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CUser

class CUserCreationForm(UserCreationForm):

    class Meta:
        model = CUser
        fields = ('username', 'email')

class CUserChangeForm(UserChangeForm):

    class Meta:
        model = CUser
        fields = ('username', 'email')