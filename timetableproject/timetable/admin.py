from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import CUserCreationForm, CUserChangeForm
from .models import CUser

class CustomUserAdmin(UserAdmin):
    add_form = CUserCreationForm
    form = CUserChangeForm
    model = CUser
    list_display = ['email', 'username',]

admin.site.register(CUser, CustomUserAdmin)